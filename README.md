Design your own Harlem Shake! Use any animation you like! Use any music you like! All customizable in the settings/ini file!

How to use it:
Press Right Control + H to start your custom Harlem Shake.

What you can change in the ini file:
The start Harlem Shake keys.
The sound file to play.
Whether to fade out music or instantly stop when at the end.
The first animation to play.
Whether to play the first animation or not.
The second animation to play.
When to play the second animation.
When to stop animation/music after start.
Whether to play the animations/create explosions at end if set to on nearby pedestrians as well.
Whether or not to spawn an explosion(s).


How to install:
First install .NET Scripthook.
Copy DesignYourOwnHarlemShake.net.dll and DesignYourOwnHarlemShake.ini to the scripts folder in the GTA IV directory.
Customize the ini file for animation(s) to play, music to play, etc. (unless you are going to use an example custom Harlem Shake. If so, then follow the instructions in the examples folder.)

Changelog:
v1.1:
Added the ability to make nearby pedestrians do the animations as well.
Added the explode option.
Added the directory shortcuts ( [MusicDirectory] and [GTAIVDirectory] ).
Bug fix: If the script crashes, you use the command 'abortscripts', or the game closes, the music will stop playing.
Lots of other changes in the code.

Notes:
For the music file location in the settings file, if you include [MusicDirectory], the mod will replace that with your 'My Music' directory. If you include [GTAIVDirectory], the mod will replace that with the GTA IV directory.

Thanks:
Aru: Scripthook
HazardX: .NET Scripthook

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with Main.cs
* create a new CS source file in Visual Studio and replace it with MyStuff.cs
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
