﻿using GTA;
using System;
using System.Net;
using System.Xml;

namespace MyStuff
{
    public class PositionAndSize
    {
        public Vector2 Position, Size;

        public PositionAndSize(Vector2 position, Vector2 size)
        {
            Position = position;
            Size = size;
        }
    }

    public static class MyExtensions
    {
        public static bool isTouchingAnyVehicle(this Vehicle veh)
        {
            bool Is = false;
            foreach (Vehicle v in World.GetVehicles(veh.Position, 40.0f))
            {
                if (veh.isTouching(v))
                {
                    Is = true;
                    break;
                }
            }
            return Is;
        }

        public static double DifferenceMS(this TimeSpan thisTs, TimeSpan ts)
        {
            return +thisTs.Subtract(ts).TotalMilliseconds;
        }
    }

    public class MyFunctions
    {
        public static void Subtitle(string message, uint time = 1500)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", message, time, 1);
        }

        public static bool imOnline()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
