﻿using GTA;
using System;
using System.IO;
using System.Windows.Forms;

namespace DesignYourOwnHarlemShake
{
    public class DesignYourOwnHarlemShake_Script : Script
    {
        /*
        Anim Flags
        Unknown12 = 2048,
		Unknown11 = 1024,
		Unknown10 = 512,
		Unknown09 = 256,
		Unknown08 = 128,
		Unknown07 = 64,
		Unknown06 = 32,
		Unknown05 = 16,
		Unknown04 = 8,
		Unknown03 = 4,
		Unknown02 = 2,
		Unknown01 = 1,
		None = 0 
         */

        AnimationSet animationSet1, animationSet2/* = new AnimationSet("swimming")*/; //Customize anim set
        Keys startHoldKey, startPressKey;
        string soundFileLocation, animName1, animName2;
        int punchlineTime, timeAfterPunchline;
        bool playAnimOne, doForPedsAround, fadeMusic, explode;
        Ped[] aroundPeds;

        public DesignYourOwnHarlemShake_Script()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("START_HARLEM_SHAKE_HOLD_KEY", "KEYS", Keys.RControlKey);
                Settings.SetValue("START_HARLEM_SHALE_PRESS_KEY", "KEYS", Keys.H);

                Settings.SetValue("SOUND_FILE_LOCATION", "MUSIC", @"[MusicDirectory]\soundfile.mp3");
                Settings.SetValue("FADE_MUSIC", "MUSIC", false);

                Settings.SetValue("ANIM_SET", "ANIMATION_ONE_OPTIONS", "");
                Settings.SetValue("ANIM_NAME", "ANIMATION_ONE_OPTIONS", "");
                Settings.SetValue("PLAY_ANIM_ONE", "ANIMATION_ONE_OPTIONS", false);

                Settings.SetValue("ANIM_SET", "ANIMATION_TWO_OPTIONS", "");
                Settings.SetValue("ANIM_NAME", "ANIMATION_TWO_OPTIONS", "");
                Settings.SetValue("PUNCHLINE_TIME_MS", "ANIMATION_TWO_OPTIONS", 0000);
                Settings.SetValue("TIME_AFTER_PUNCHLIME_STOP", "ANIMATION_TWO_OPTIONS", 0000);

                Settings.SetValue("DO_FOR_PEDS_AROUND_ALSO", "OTHER", true);

                Settings.SetValue("EXPLODE", "END", true);
                Settings.Save();
            }

            startHoldKey = Settings.GetValueKey("START_HARLEM_SHAKE_HOLD_KEY", "KEYS", Keys.RControlKey);
            startPressKey = Settings.GetValueKey("START_HARLEM_SHALE_PRESS_KEY", "KEYS", Keys.H);

            @soundFileLocation = Settings.GetValueString("SOUND_FILE_LOCATION", "MUSIC", @"[MusicDirectory]/soundfile.mp3"); //Cameo - Word Up.mp3

            if (@soundFileLocation.Contains("[MusicDirectory]")) @soundFileLocation = @soundFileLocation.Replace("[MusicDirectory]", Environment.SpecialFolder.MyMusic.ToString());
            else if (@soundFileLocation.Contains("[GTAIVDirectory]")) @soundFileLocation = @soundFileLocation.Replace("[GTAIVDirectory]", Environment.CurrentDirectory);

            fadeMusic = Settings.GetValueBool("FADE_MUSIC", "MUSIC", true);

            animationSet1 = new AnimationSet(Settings.GetValueString("ANIM_SET", "ANIMATION_ONE_OPTIONS", ""));
            animName1 = Settings.GetValueString("ANIM_NAME", "ANIMATION_ONE_OPTIONS", "");
            playAnimOne = Settings.GetValueBool("PLAY_ANIM_ONE", "ANIMATION_ONE_OPTIONS", true);

            animationSet2 = new AnimationSet(Settings.GetValueString("ANIM_SET", "ANIMATION_TWO_OPTIONS", ""));
            animName2 = Settings.GetValueString("ANIM_NAME", "ANIMATION_TWO_OPTIONS", "");
            punchlineTime = Settings.GetValueInteger("PUNCHLINE_TIME_MS", "ANIMATION_TWO_OPTIONS", 0000);
            timeAfterPunchline = Settings.GetValueInteger("TIME_AFTER_PUNCHLIME_STOP", "ANIMATION_TWO_OPTIONS", 0000);

            doForPedsAround = Settings.GetValueBool("DO_FOR_PEDS_AROUND_ALSO", "OTHER", true);

            explode = Settings.GetValueBool("EXPLODE", "END", true);

            KeyDown += DesignYourOwnHarlemShake_Script_KeyDown;
        }

        private void DesignYourOwnHarlemShake_Script_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if (isKeyPressed(startHoldKey) && e.Key == startPressKey && !string.IsNullOrWhiteSpace(@soundFileLocation))
            {
                if (File.Exists(@soundFileLocation))
                {
                    WMPLib.WindowsMediaPlayer wplayer = new WMPLib.WindowsMediaPlayer();

                    wplayer.URL = @soundFileLocation;
                    wplayer.controls.play();
                    try
                    {
                        if (playAnimOne)
                        {
                            if (Game.Exists(Player.Character) && (Player.Character.isAlive || Player.Character.Health > 0))
                            {
                                TaskAnim(Player.Character, 1);
                            }
                            aroundPeds = World.GetPeds(Player.Character.Position, 75.0f);
                            if (doForPedsAround && aroundPeds.Length > 0)
                            {
                                foreach (Ped p in aroundPeds)
                                {
                                    if (Game.Exists(p) && (p.isAlive || p.Health > 0) && p != Player.Character)
                                    {
                                        TaskAnim(p, 1);
                                    }
                                }
                            }
                        }

                        TimeSpan startTime = DateTime.Now.TimeOfDay;
                        while ((DateTime.Now.TimeOfDay - startTime).TotalMilliseconds < punchlineTime) Wait(0);

                        if (Game.Exists(Player.Character) && Player.Character.isAlive && Player.Character.Animation.isPlaying(animationSet1, animName1)) Player.Character.Task.ClearAllImmediately();
                        aroundPeds = World.GetPeds(Player.Character.Position, 75.0f);

                        if (doForPedsAround && aroundPeds.Length > 0)
                        {
                            foreach (Ped p in aroundPeds)
                            {
                                if (Game.Exists(p) && (p.isAlive || p.Health > 0) && p != Player.Character)
                                {
                                    TaskAnim(p, 2);
                                }
                            }
                        }

                        if ((Game.Exists(Player.Character) && (Player.Character.isAlive || Player.Character.Health > 0))) TaskAnim(Player.Character, 2);

                        TimeSpan playAnimTime = DateTime.Now.TimeOfDay;

                        while ((DateTime.Now.TimeOfDay - playAnimTime).TotalMilliseconds < timeAfterPunchline) Wait(0);
                        if (fadeMusic)
                        {
                            for (int i = 100; i > 0; i--)
                            {
                                wplayer.settings.volume = i;
                                Wait(20);
                            }
                        }
                        wplayer.controls.stop();

                        if (Game.Exists(Player.Character) && (Player.Character.isAlive || Player.Character.Health > 0) && Player.Character.Animation.isPlaying(animationSet2, animName2)) Player.Character.Task.ClearAllImmediately();

                        aroundPeds = World.GetPeds(Player.Character.Position, 75.0f);

                        if (doForPedsAround && aroundPeds.Length > 0)
                        {
                            foreach (Ped p in aroundPeds)
                            {
                                if (Game.Exists(p) && p != Player.Character && (p.isAlive || p.Health > 0))
                                {
                                    p.Task.ClearAllImmediately();
                                    if (explode)
                                    {
                                        World.AddExplosion(p.Position);
                                        Wait(50);
                                    }
                                }
                            }
                        }

                        if (explode)
                        {
                            if (Game.Exists(Player.Character) && (Player.Character.isAlive || Player.Character.Health > 0)) World.AddExplosion(Player.Character.Position);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (wplayer != null && wplayer.playState == WMPLib.WMPPlayState.wmppsPlaying) wplayer.controls.stop();
                        throw ex;
                    }

                    wplayer = null;
                }
                else
                {
                    throw new NonExistingObjectException(string.Format("The sound file '{0}' does not exist", @soundFileLocation));
                }
            }
        }

        private void TaskAnim(Ped ped, int whichAnim)
        {
            Vehicle currentVehicle = ped.CurrentVehicle;
            bool checkForVeh = (currentVehicle != null && ped.isInVehicle());

            TaskSequence ts = new TaskSequence();

            if (checkForVeh)
            {
                currentVehicle.Speed = 0;

                ts.AddTask.LeaveVehicle(currentVehicle, true);
            }
            else
            {
                ped.Task.ClearAllImmediately();
            }

            AnimationSet animSetToUse = new AnimationSet("swimming");
            string animNameToUse = "idle";

            if (whichAnim == 1)
            {
                animSetToUse = animationSet1;
                animNameToUse = animName1;
            }
            else if (whichAnim == 2)
            {
                animSetToUse = animationSet2;
                animNameToUse = animName2;
            }

            ts.AddTask.PlayAnimation(animSetToUse, animNameToUse, 8.0f, AnimationFlags.Unknown05/*repeat*/);

            ts.Perform(ped);
        }
    }
}
